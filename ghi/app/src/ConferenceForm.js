import React, { useEffect, useState } from 'react';

function ConferenceForm () {

    const handleSubmit = async (event) => {
        event.preventDefault();

        const data = {};

        data.name = name;
        data.starts = starts;
        data.ends = ends;
        data.description = description;
        data.max_presentations = max_presentations;
        data.max_attendees = max_attendees;
        data.location = location;

        console.log(data)

        const conferenceUrl = 'http://localhost:8000/api/conferences/';
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        };

        const response = await fetch(conferenceUrl, fetchConfig);
        if (response.ok) {
            const newConference = await response.json();
            console.log(newConference);

            setName('');
            setStart('');
            setEnd('');
            setDescription('');
            setMaxPres('');
            setMaxAttend('');
            setLocation('');

        }

    }

    const [locations, setLocations] = useState([]);
    const [name, setName] = useState('');
    const [starts, setStart] = useState('');
    const [ends, setEnd] = useState('');
    const [description, setDescription] = useState('');
    const [max_presentations, setMaxPres] = useState('');
    const [max_attendees, setMaxAttend] = useState('');
    const [location, setLocation] = useState('');

    const handleNameChange = (event) => {
        const value = event.target.value;
        setName(value);
    }
    const handleStartChange = (event) => {
        const value = event.target.value;
        setStart(value);
    }
    const handleEndChange = (event) => {
        const value = event.target.value;
        setEnd(value);
    }
    const handleDescriptionChange = (event) => {
        const value = event.target.value;
        setDescription(value);
    }
    const handleMaxPresChange = (event) => {
        const value = event.target.value;
        setMaxPres(value);
    }
    const handleMaxAttendChange = (event) => {
        const value = event.target.value;
        setMaxAttend(value);
    }
    const handleLocationChange = (event) => {
        const value = event.target.value;
        setLocation(value);
    }

    const fetchData = async () => {
        const url = "http://localhost:8000/api/locations/";

        const response = await fetch(url);

        if (response.ok) {
            const data = await response.json();
            setLocations(data.locations)
        }
    }

    useEffect(() => {
        fetchData();
    }, []);

    return(
        <div className="row">
            <div className="offset-3 col-6">
            <div className="shadow p-4 mt-4">
                <h1>Create a new conference</h1>
                <form onSubmit={handleSubmit} id="create-con-form">
                <div className="form-floating mb-3">
                    <input value={name} onChange={handleNameChange} placeholder="Name" required type="text" id="name" name="name" className="form-control" />
                    <label htmlFor="name">Name</label>
                </div>
                <div className="form-floating mb-3">
                    <input value={starts} onChange={handleStartChange} placeholder="Starts" required type="date" id="starts" name="starts" className="form-control" />
                    <label htmlFor="starts">Starts</label>
                </div>
                <div className="form-floating mb-3">
                    <input value={ends} onChange={handleEndChange} placeholder="Ends" required type="date" id="ends" name="ends" className="form-control" />
                    <label htmlFor="ends">Ends</label>
                </div>
                <div className="mb-3">
                    <label htmlFor="description" className="form-label">Description</label>
                    <textarea value={description} onChange={handleDescriptionChange} className="form-control" required name="description" id="description" rows="5"></textarea>
                </div>
                <div className="form-floating mb-3">
                    <input value={max_presentations} onChange={handleMaxPresChange} placeholder="Maximum presentations" required type="number" id="maxPres" name="max_presentations" className="form-control" />
                    <label htmlFor="maxPres">Maximum presentations</label>
                </div>
                <div className="form-floating mb-3">
                    <input value={max_attendees} onChange={handleMaxAttendChange} placeholder="Maximum attendees" required type="number" id="maxAttend" name="max_attendees" className="form-control" />
                    <label htmlFor="maxAttend">Maximum attendees</label>
                </div>
                <div className="mb-3">
                    <select value={location} onChange={handleLocationChange} required id="location" name="location" className="form-select">
                        <option value="">Choose a location</option>
                        {locations.map(location => {
                            return (
                                <option key={location.id} value={location.id}>
                                    {location.name}
                                </option>
                            )
                        })}
                    </select>
                </div>
                <button className="btn btn-primary">Create</button>
                </form>
            </div>
            </div>
        </div>
    )
}

export default ConferenceForm
