import React, { useEffect, useState } from 'react';

function PresentationForm () {

    const [name, setName] = useState('');
    const [email, setEmail] = useState('');
    const [companyName, setCompanyName] = useState('');
    const [title, setTitle] = useState('');
    const [synopsis, setSynopsis] = useState('');
    const [conference, setConference] = useState('');
    const [conferences, setConferences] = useState([]);

    const fetchData = async () => {
        const url = "http://localhost:8000/api/conferences/";
        const response = await fetch(url);
        if (response.ok) {
            const data = await response.json();
            setConferences(data.conferences);
        }
    }

    useEffect(() => {
        fetchData();
    }, []);

    const handleNameChange = (event) => {
        const value = event.target.value;
        setName(value);
    }
    const handleEmailChange = (event) => {
        const value = event.target.value;
        setEmail(value);
    }
    const handleCompanyNameChange = (event) => {
        const value = event.target.value;
        setCompanyName(value);
    }
    const handleTitleChange = (event) => {
        const value = event.target.value;
        setTitle(value);
    }
    const handleSynopsisChange = (event) => {
        const value = event.target.value;
        setSynopsis(value);
    }
    const handleConferenceChange = (event) => {
        const value = event.target.value;
        setConference(value);
    }

    const handleSubmit = async (event) => {
        event.preventDefault();
        const data = {};
        data.presenter_name=name;
        data.presenter_email=email;
        data.company_name=companyName;
        data.title=title;
        data.synopsis=synopsis;
        data.conference=conference;

        const selectTag = document.getElementById("conference");
        const selectIndex = selectTag.value
        const presentationUrl = `http://localhost:8000/api/conferences/${selectIndex}/presentations/`;
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        };

        const response = await fetch(presentationUrl, fetchConfig);
        if (response.ok) {
            setName('');
            setEmail('');
            setCompanyName('');
            setTitle('');
            setSynopsis('');
            setConference('');
        }
    }

    return (
    <div className="row">
        <div className="offset-3 col-6">
          <div className="shadow p-4 mt-4">
            <h1>Create a new presentation</h1>
            <form onSubmit={handleSubmit} id="create-present-form">
              <div className="form-floating mb-3">
                <input value={name} onChange={handleNameChange} placeholder="Presenter name" required type="text" id="name" name="presenter_name" className="form-control" />
                <label htmlFor="name">Presenter name</label>
              </div>
              <div className="form-floating mb-3">
                <input value={email} onChange={handleEmailChange} placeholder="Presenter email" required type="email" id="presenter_email" name="presenter_email" className="form-control" />
                <label htmlFor="presenter_email">Presenter email</label>
              </div>
              <div className="form-floating mb-3">
                <input value={companyName} onChange={handleCompanyNameChange} placeholder="Company name" type="text" id="company_name" name="company_name" className="form-control" />
                <label htmlFor="company_name">Company name</label>
              </div>
              <div className="form-floating mb-3">
                <input value={title} onChange={handleTitleChange} placeholder="Title" required type="text" id="title" name="title" className="form-control" />
                <label htmlFor="title">Title</label>
              </div>
              <div className="mb-3">
                <label htmlFor="synopsis" className="form-label">Synopsis</label>
                <textarea value={synopsis} onChange={handleSynopsisChange} className="form-control" required name="synopsis" id="synopsis" rows="5"></textarea>
              </div>
              <div className="mb-3">
                <select value={conference} onChange={handleConferenceChange} required id="conference" name="conference" className="form-select">
                  <option value="">Choose a conference</option>
                  {conferences.map(conference => {
                    return (
                        <option key={conference.id} value={conference.id}>
                            {conference.name}
                        </option>
                    )
                  })}
                </select>
              </div>
              <button className="btn btn-primary">Create</button>
            </form>
          </div>
        </div>
    </div>
    )
}

export default PresentationForm
